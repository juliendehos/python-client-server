# Client-server in python

```
$ ./server.py 
begin server
sending: hello from server
received: hello from client 52458
sending: hello from server
received: hello from client 52460
```

```
$ ./client.py 
received: hello from server
sending: hello from client 52458
```

```
$ ./client.py 
received: hello from server
sending: hello from client 52460
```

