#!/usr/bin/env python3

import socket

HOST, PORT = '', 8000

with socket.socket() as client_socket:

    # connect to the server
    client_socket.connect((HOST, PORT))
    (_, port) = client_socket.getsockname()

    # receive data from the server
    received_data = str(client_socket.recv(1024), 'utf-8')
    print('received:', received_data)

    # send data to the server
    sent_data = 'hello from client'
    print('sending:', sent_data, port)
    client_socket.sendall(bytes(sent_data, 'utf-8'))

