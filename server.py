#!/usr/bin/env python3

import socket

HOST, PORT = '', 8000

print('begin server')

# create listening socket
with socket.socket() as server_socket:
    server_socket.bind((HOST, PORT))
    server_socket.listen(5)

    # handle client connections
    try:
        while True:
            client, (_, port) = server_socket.accept()
            with client:
                # send data
                sent_data = 'hello from server'
                print('sending:', sent_data)
                client.sendall(bytes(sent_data, 'utf-8'))

                # receive data
                received_data = str(client.recv(1024).strip(), 'utf-8')
                print('received:', received_data, port)

    # end server if C-c
    except (KeyboardInterrupt, SystemExit):
        print('')
        print('end server')

